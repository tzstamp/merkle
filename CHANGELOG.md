# Changelog
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added
- Rollup build script
  - CommonJS output to `/dist/common.js`
- Export utility functions in root namespace

### Changed
- Rewrite source code in TypeScript
- Rewrite APIs to use asynchronous hashing
  - Compatible with Web Crypto API

### Removed
- MerkleTree appends during initiation
- `Proof.prototype.verify` verification shortcut
  - Method name to be reused for notary verification

## [1.1.0] - 2020-11-19
### Added
- Proof.parse static convenience method to deserialize a JSON proof

### Fixed
- MerkleTree.append properly deduplicates hashes

## [1.0.0] - 2020-11-18
### Added
- Proofs
  - Constructable from and serializable as JSON
  - Does not contain the document hash or Merkle root
  - Derive the Merkle root given a document hash
  - Verify the proof given the Merkle root and document hash
- MerkleTrees
  - Fast-append document hashes
  - Access document hashes in an array
  - Build proofs given a document hash

[1.1.0]: https://gitlab.com/tzstamp/merkle/-/releases#1.1.0
[1.0.0]: https://gitlab.com/tzstamp/merkle/-/releases#1.0.0
