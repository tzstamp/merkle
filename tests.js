const assert = require('assert')
const {
  MerkleTree,
  Proof,
  compare,
  hash,
  concat,
  stringify,
  parse,
  hashOf,
  sizeOf,
  node,
  append
} = require('./dist/common.js')

const utf8 = new TextEncoder
const zerolead = utf8.encode('zerolead')
const zeroleadHash = hash(zerolead)
const zeroleadArray = [
  0x00, 0xda, 0x6c, 0x88, 0x35, 0xa0, 0x28, 0xb1,
  0x4c, 0xe4, 0xa9, 0x0e, 0x77, 0xa3, 0x29, 0x4c,
  0x95, 0x8a, 0x70, 0xf5, 0x12, 0x33, 0x35, 0x60,
  0x12, 0x16, 0xa5, 0x35, 0x71, 0xd2, 0x44, 0x9d
]
const zeroleadHex = '00da6c8835a028b14ce4a90e77a3294c958a70f5123335601216a53571d2449d'
const hashPool = new Array(256)
  .fill(0)
  .map((_, i) => new Uint8Array([i]))
  .map(hash)

void async function () {
  testCompare()
  await testHash()
  await testConcat()
  await testStringify()
  await testParse()
  await testHashOf()
  await testSizeOf()
  await testNode()
  await testAppend()
  await testProof()
  await testMerkleTree()
  console.log('All tests passed')
}()

// compare
function testCompare () {
  const baseArray = new Uint8Array([ 1, 2, 3 ])
  const altArray = new Uint8Array([ 1, 3, 5 ])
  const longerArray = new Uint8Array([ 1, 2, 3, 4 ])
  const emptyArray = new Uint8Array
  assert.ok(compare(baseArray, baseArray), 'Bad compare() output')
  assert.ok(!compare(baseArray, altArray), 'Bad compare() output')
  assert.ok(!compare(baseArray, longerArray), 'Bad compare() output')
  assert.ok(compare(emptyArray, emptyArray), 'Bad compare() output')
}

// hash
async function testHash () {
  assert.ok(compare(await zeroleadHash, zeroleadArray), 'Bad hash() output')
}

// concat
async function testConcat () {
  const [ h1, h2 ] = await Promise.all(hashPool.slice(0, 2))
  const array = [
    0x30, 0xe1, 0x86, 0x74, 0x24, 0xe6, 0x6e, 0x8b,
    0x6d, 0x15, 0x92, 0x46, 0xdb, 0x94, 0xe3, 0x48,
    0x67, 0x78, 0x13, 0x6f, 0x7e, 0x38, 0x6f, 0xf5,
    0xf0, 0x01, 0x85, 0x9d, 0x6b, 0x84, 0x84, 0xab
  ]
  const root = await concat(h1, h2)
  assert.ok(compare(root, array), 'Bad concat() output')
}

// stringify
async function testStringify () {
  assert.strictEqual(stringify(await zeroleadHash), zeroleadHex, 'Bad stringify() output')
}

// parse
async function testParse () {
  assert.ok(compare(parse(zeroleadHex), await zeroleadHash), 'Bad parse() output')
  assert.throws(() => parse('badinput'), 'parse() failed to throw exception on invalid input')
}

// hashOf
async function testHashOf () {
  assert.ok(compare(hashOf(await zerolead), hashOf({ hash: await zerolead })), 'Bad hashOf() output')
}

// sizeOf
async function testSizeOf () {
  assert.strictEqual(sizeOf(await zerolead), sizeOf({ size: 1 }), 'Bad sizeOf() output')
}

// node
async function testNode () {
  const [ h1, h2, h3 ] = await Promise.all(hashPool.slice(0, 3))
  const n1 = await node(h1, h2)
  const n2 = await node(n1, h3)
  const r1 = await concat(h1, h2)
  const r2 = await concat(r1, h3)
  assert.ok(n2.size == 3, 'Bad node() size value')
  assert.ok(compare(n2.hash, r2), 'Bad node() hash value')
}

// append
async function testAppend () {
  const [ h1, h2, h3, h4 ] = await Promise.all(hashPool.slice(0, 4))
  const n1 = await append(h1, h2)
  const n2 = await append(n1, h3)
  const n3 = await append(n2, h4)
  assert.ok(n1.left == h1 && n1.right == h2, 'Bad append() output')
  assert.ok(n2.left == n1 && n2.right == h3, 'Bad append() output')
  assert.ok(n3.left == n2.left && n3.right.left == n2.right && n3.right.right == h4, 'Bad append() output')
}

// Proof
async function testProof () {
  const [ h1, h2, h3, h4 ] = await Promise.all(hashPool.slice(0, 4))
  const r1 = await concat(h1, h2)
  const r2 = await concat(h3, h4)
  const r3 = await concat(r1, r2)
  const p1 = new Proof({ size: 2, index: 0, path: [ h2 ] })
  const p2 = new Proof({ size: 4, index: 2, path: [ r1, h4 ] })
  const p3 = Proof.parse(JSON.stringify(p2))
  const d1 = await p1.derive(h1)
  const d2 = await p2.derive(h3)
  assert.ok(compare(d1, r1), 'Bad Proof.prototype.verify() output')
  assert.ok(compare(d2, r3), 'Bad Proof.prototype.verify() output')
  assert.ok(p3.size == 4, 'Bad serialization and deserialization size value')
  assert.ok(p3.index == 2, 'Bad serialization and deserialization index value')
  for (const index in p2.path) {
    const step1 = p2.path[index]
    const step2 = p3.path[index]
    assert.ok(compare(step1, step2), 'Bad serialization and deserialization path value')
  }
}

// MerkleTree
async function testMerkleTree () {
  const [ h1, h2 ] = await Promise.all(hashPool.slice(0, 2))
  const t1 = new MerkleTree
  const t2 = new MerkleTree
  const t3 = new MerkleTree
  const r1 = await concat(h1, h2)
  await t1.append(h1, h1, h2)
  await t2.append(...await Promise.all(hashPool))
  assert.ok(sizeOf(t1.root) == 2, 'Append deduplication failed')
  assert.ok(compare(t1.hash, r1), 'Bad MerkleTree.prototype.hash value')
  assert.ok(sizeOf(t2.root) == hashPool.length, 'Bad MerkleTree.prototype.root.size value')
  for (const leaf of t2.leaves) {
    const proof = t2.prove(leaf)
    const root = await proof.derive(leaf)
    assert.ok(compare(root, t2.hash), 'Failed to derive good merkle root')
  }
}
