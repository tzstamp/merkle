# Merkle
TzStamp Merkle tree and proofs

## Installation
```
$ npm install --save @tzstamp/merkle
```

## `Proof` class
Serializable Merkle proof

```js
// Import
const { Proof } = require('@tzstamp/merkle')

// Deserialize from JSON
const proofString = fs.readFileSync('./my-proof.json')
const proof = Proof.parse(proofString)

// Serialize as JSON
const proofJSON = JSON.stringify(proof)
```

### `proof.derive(hash: Uint8Array): Promise<Uint8Array>`
Derive the Merkle root, given the leaf hash.

## `MerkleTree` class
Appendable Merkle tree

```js
// Import
const { MerkleTree } = require('@tzstamp/merkle')

// Instantiate empty tree
const merkleTree = new MerkleTree
```

### `merkleTree.append(...hashes: Uint8Array[]): Promise<void>`
Append leaves to the merkle tree. Deduplicates leaves.

### `merkleTree.prove(hash: Uint8Array): Proof`
Build a proof, given a leaf hash included in the tree. Throws an error when the leaf is not included in the tree.

### `merkleTree.leaves: Uint8Array[]`
All leaves included in the tree in append order.

## License
[MIT](LICENSE.txt) &copy; 2020–2021 John David Pressman, Benjamin Herman
