/**
 * Compare two unsigned 8-bit integer arrays
 */
export function compare (a: Uint8Array, b: Uint8Array): boolean {
  return a.length == b.length && a.every((val, idx) => val == b[idx]);
}
