import { parse, concat, stringify } from './hash';

/**
 * Proof constructor options
 */
type ProofOptions = {
  size: number,
  index: number,
  path: Uint8Array[]
};

/**
 * Proof serialization structure
 */
type ProofSerialization = {
  size: number,
  index: number,
  path: string[]
};

/**
 * Merkle proof
 */
export class Proof {

  /**
   * Size of the original Merkle tree
   */
  size: number;

  /**
   * Index of the leaf within the original tree
   */
  index: number;

  /**
   * Sibling hashes along the traversal path to the root
   */
  path: Uint8Array[];

  constructor ({ size, index, path }: ProofOptions) {
    this.size = size;
    this.index = index;
    this.path = path;
  }

  /**
   * Deserialize a Proof from JSON
   */
  static parse (string: string): Proof {
    const data: ProofSerialization = JSON.parse(string);
    const path = data.path.map(parse);
    return new Proof({
      size: data.size,
      index: data.index,
      path
    });
  }

  /**
   * Derive the Merkle root hash given a leaf hash
   * @param hash Leaf hash
   */
  derive (hash: Uint8Array): Promise<Uint8Array> {

    // Relative step index and Merkle node size
    // Starts at root of tree
    let { size, index } = this;

    // Map each path step left or right
    const dir = this.path.map(() => {

      // Size of left branch
      const leftSize = 2 ** Math.floor(Math.log2(size - 1));

      // Step is right (true) or left (false)
      return index >= leftSize
        ? (size -= leftSize, index -= leftSize, true)
        : (size = leftSize, false);
    });

    // Reduce path by concatenation
    // Determine append (true) or prepend (false) from directions mapping
    const reducer = async (acc: Promise<Uint8Array>, cur: Uint8Array, idx: number) =>
      dir[idx]
        ? concat(cur, await acc)
        : concat(await acc, cur);

    // Path contains sibling hashes calculated root-to-leaf
    // Reduce right to derive leaf-to-root
    return this.path.reduceRight(reducer, Promise.resolve(hash));
  }

  /**
   * JSON serializer
   */
  toJSON (): ProofSerialization {
    return {
      size: this.size,
      index: this.index,
      path: this.path.map(stringify)
    }
  }
}
