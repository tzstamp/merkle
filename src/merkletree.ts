import { Proof } from './proof';
import { compare } from './compare';
import { concat } from './hash'

/**
 * Get the hash value of a Merkle node
 */
export function hashOf (node: MerkleNode) {
  return node instanceof Uint8Array
    ? node
    : node.hash;
}

/**
 * Get the size of a Merkle leaf
 */
export function sizeOf (node: MerkleNode) {
  return node instanceof Uint8Array
    ? 1
    : node.size;
}

/**
 * Produce a Merkle node
 */
export async function node (left: MerkleNode, right: MerkleNode): Promise<MerkleNode> {
  return {
    hash: await concat(
      hashOf(left),
      hashOf(right)
    ),
    size: sizeOf(left) + sizeOf(right),
    left,
    right
  };
}

/**
 * Append an unsigned 8-bit integer array to a merkle node
 * @param tree Merkle node structure or leaf
 * @param leaf Leaf to append
 */
export async function append (tree: MerkleNode, leaf: Uint8Array): Promise<MerkleNode> {

  // Append a leaf to a leaf
  // Yields a perfect tree
  if (tree instanceof Uint8Array) return node(tree, leaf);

  // Append a leaf to a perfect tree
  // Yields an unbalanced tree
  if (sizeOf(tree.left) == sizeOf(tree.right)) return node(tree, leaf);

  // Append a leaf to an unbalanced tree
  // Recurse down right branch until appending to a leaf or perfect tree
  return node(tree.left, await append(tree.right, leaf));
}

/**
 * Merkle node
 */
export type MerkleNode = Uint8Array | {
  size: number,
  hash: Uint8Array,
  left: MerkleNode | Uint8Array,
  right: MerkleNode | Uint8Array
};

/**
 * Merkle tree container
 */
export class MerkleTree {

  /**
   * Merkle root
   */
  root: null | Uint8Array | MerkleNode = null;

  /**
   * Ordered list of leaves in the tree
   */
  leaves: Uint8Array[] = [];

  /**
   * Merkle root hash
   */
  get hash (): Uint8Array {
    if (this.root == null) return null;
    return hashOf(this.root);
  }

  /**
   * Append leaves to the tree
   */
  async append (...hashes: Uint8Array[]): Promise<void> {
    for (const hash of hashes) {

      // Deduplicate leaves already in tree
      if (this.leaves.find(leaf => compare(leaf, hash))) continue;

      // Special case for first leaf
      if (this.root == null) this.root = hash;

      // Subsequent leaves
      else this.root = await append(this.root, hash);

      // Remember appended leaves
      this.leaves.push(hash);
    }
  }

  /**
   * Build a proof for a given hash in the tree
   * @throws when the hash isn't included in the tree
   */
  prove (hash: Uint8Array): Proof {

    // Get leaf position in tree
    const index = this.leaves.findIndex(leaf => compare(hash, leaf));
    if (index == -1) throw new Error('Leaf hash not found in merkle tree');

    // Relative index and Merkle node
    // Starts at root of tree
    let relIndex = index;
    let relTree = this.root;

    // Merkle node hashes needed to rederive the Merkle root from the target leaf
    const path: Uint8Array[] = [];

    // Iterate deeper into tree until the relative Merkle node is the leaf in target
    while (!(relTree instanceof Uint8Array)) {

      // Target leaf is in the right branch
      if (relIndex >= sizeOf(relTree.left)) {
        path.push(hashOf(relTree.left));
        relIndex -= sizeOf(relTree.left);
        relTree = relTree.right;
      }

      // Target leaf is in the left branch
      else {
        path.push(hashOf(relTree.right));
        relTree = relTree.left;
      }
    }

    return new Proof({
      size: sizeOf(this.root),
      index,
      path
    });
  }
}
