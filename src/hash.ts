import { createHash } from 'crypto';

async function hashCommon (data: Uint8Array): Promise<Uint8Array> {
  const digest = createHash('SHA256')
    .update(data)
    .digest();
  return new Uint8Array(digest);
}

async function hashWeb (data: Uint8Array): Promise<Uint8Array> {
  const digest = await crypto.subtle.digest('SHA-256', data);
  return new Uint8Array(digest);
}

/**
 * Produce SHA-256 digest
 * @param data Input data
 */
export const hash = typeof window == 'undefined'
  ? hashCommon
  : hashWeb;

/**
 * SHA-256 hash-concatenate two unsigned 8-bit integer arrays
 */
export async function concat (left: Uint8Array, right: Uint8Array): Promise<Uint8Array> {
  const array = new Uint8Array([ ...left, ...right ]);
  return await hash(array);
}

/**
 * Convert SHA-256 unsigned 8-bit integer array digest to hexadecimal string
 * @param hash
 */
export function stringify (hash: Uint8Array): string {
  return Buffer
    .from(hash)
    .toString('hex')
    .padStart(64, '0');
}

/**
 * Parse hexadecimal string to unsigned 8-bit integer array
 * @throws when hash string is invalid
 */
export function parse (string: string) {
  if (/^[0-9a-fA-F]+$/.test(string) && string.length % 2 == 0) {
    const bytes = string
      .match(/.{1,2}/g)
      .map(byte => parseInt(byte, 16));
    return new Uint8Array(bytes);
  }
  throw new Error('Invalid hash string');
}
